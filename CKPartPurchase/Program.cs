﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using CKWIS.Support.Helpers;
using RestDemo.Models;
using RestDemo.Helpers;
using System.Net.Http;

namespace RestDemo
{
    class Program
    {
        private static int lClaimID;
        private static int lClaimDetailID;
        private static int lUserID;
        private static string sCON;
        private static string sVIN;
        private static string SQL;

        public static double CastDouble(string value, double defaultValue)
        {
            double item = 0;
            if (double.TryParse(value, out item))
            {
                return item;

            }
            return defaultValue;
        }

        static async Task Main(string[] args)
        {
            lClaimID = int.Parse(args[0]);
            lUserID = int.Parse(args[1]);

            sCON = "server=198.143.98.122;database=veritas;User Id=sa;Password=NCC1701E";

            var username = ConfigurationManager.AppSettings["UserName"];
            var pwd = ConfigurationManager.AppSettings["Password"];
            var grantType = ConfigurationManager.AppSettings["grant_type"];
            var tokenRoute = ConfigurationManager.AppSettings["TokenRoute"];
            var baseEndpoint = ConfigurationManager.AppSettings["BaseEndPoint"];
            var quote = ConfigurationManager.AppSettings["Quote"];
            var makes = ConfigurationManager.AppSettings["Makes"];
            var placeOrder = ConfigurationManager.AppSettings["PlaceOrder"];
            var tokenTimeout = TimeSpan.FromSeconds(CastDouble(ConfigurationManager.AppSettings["TokenRequestTimeout"], 3));
            var basicTimeout = TimeSpan.FromSeconds(CastDouble(ConfigurationManager.AppSettings["RequestTimeout"], 60));

            var restClient = GetRestHelper();
            var config = new CKWebapiHTTPTokenProviderConfig
            {
                BaseAddress = new Uri(baseEndpoint),
                TokenReq = new TokenRequest(username, pwd, grantType),
                TokenRoute = tokenRoute,
                TokenTimeout = tokenTimeout

            };

            var tokenProvider = GetTokenProvider(config, restClient);


            var makesResult = await GetMakes(restClient, tokenProvider, basicTimeout, baseEndpoint, makes);

            var quoteResult = await GetQuote(restClient, tokenProvider, basicTimeout, baseEndpoint, quote);
            //create parts list, you can add more than one






            var partRequests = quoteResult.oem.Select(p => new OrderRequestPart { aftermarketoption = true, partnumber = p.partnumber, quoteid = p.quoteid, quantity = 1 }).ToArray();
            //OrderRequestPart[] partRequests = new OrderRequestPart[0];
            var orderResponse = await PlaceOrder(restClient, tokenProvider, basicTimeout, baseEndpoint, placeOrder, partRequests);
            DBO.clsDBO clR = new DBO.clsDBO();

            SQL = "select * from claimdetailpurchase " +
                "where claimdetailquoteid = " + lClaimID;
            clR.OpenDB(SQL, sCON);
            if (clR.RowCount > 0)
            {
                clR.GetRow();
                if ( Boolean.Parse(clR.get_Fields("oem")) == false)
                {
                    clR.set_Fields("PartCost", Convert.ToString(orderResponse.aftermarketpartscost));
                    clR.set_Fields("Shipping", Convert.ToString(orderResponse.aftermarketshippingcost));
                    clR.set_Fields("orderid", orderResponse.orderid);
                    clR.set_Fields("TotalOrderCost", Convert.ToString(orderResponse.totalordercost));
                }
                else
                {
                    clR.set_Fields("PartCost", Convert.ToString(orderResponse.oempartscost));
                    clR.set_Fields("Shipping", Convert.ToString(orderResponse.oemshippingcost));
                    clR.set_Fields("orderid", orderResponse.orderid);
                    clR.set_Fields("TotalOrderCost", Convert.ToString(orderResponse.totalordercost));
                }
                clR.SaveDB();
            }


            //Console.WriteLine(orderResponse.orderid);

        }

        public static async Task<OrderResponse> PlaceOrder(IRestHelper restHelper, ITokenProvider tokenProvider, TimeSpan timeout, string baseUrl, string route,OrderRequestPart[] partRequests)
        {


            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(baseUrl);
                httpClient.Timeout = timeout;

                DBO.clsDBO clR = new DBO.clsDBO();
                DBO.clsDBO clCL = new DBO.clsDBO();
                DBO.clsDBO clU = new DBO.clsDBO();
                DBO.clsDBO clC = new DBO.clsDBO();
                DBO.clsDBO clSC = new DBO.clsDBO();
                string SQL;
                string sClaimID = "";
                string sContractID = "";
                string sServiceCenterID = "";
                string sUserName = "";
                string sUserEMail = "";
                string sContractNo = "";
                string sAuthorizationNumber = "";
                string sRepairOrderNumber = "";
                string sOwner = "";
                string sMileage = "";
                string sRepairSite = "";
                string sAddress = "";
                string sPostalCode = "";
                string sPhone = "";
                string sContact = "";
                string sWarranty = "";
                string sEocMileage = "";
                string sEocDate = "";
                string sNote = "";
                

                SQL = "select * from userinfo " +
                    "where userid = " + lUserID;
                clU.OpenDB(SQL, sCON);
                if (clU.RowCount>0)
                {
                    clU.GetRow();
                    sUserName = clU.get_Fields("FName") + " " + clU.get_Fields("lname");
                    sUserEMail = clU.get_Fields("email");
                }


                SQL = "select * from claimdetail " +
                    "where claimdetailid = " + lClaimDetailID;
                clR.OpenDB(SQL, sCON);
                if (clR.RowCount > 0)
                {
                    clR.GetRow();
                    sClaimID = clR.get_Fields("ClaimID");
                }

                SQL = "select * from claim where claimid = " + sClaimID;
                clCL.OpenDB(SQL, sCON);
                if (clCL.RowCount > 0)
                {
                    clCL.GetRow();
                    sContractID = clCL.get_Fields("ContractID");
                    sRepairOrderNumber = clCL.get_Fields("RONumber");
                    sAuthorizationNumber = clCL.get_Fields("ClaimNo");
                    sMileage = clCL.get_Fields("lossMile");
                    sServiceCenterID = clCL.get_Fields("servicecenterID");
                    try
                    {
                        sContact = clCL.get_Fields("SCContactInfo");
                    }
                    catch
                    {                        
                    }
                    
                }

                SQL = "select * from contract where contractid = " + sContractID;
                clC.OpenDB(SQL,sCON);
                if (clC.RowCount > 0)
                {
                    clC.GetRow();
                    sContractNo = clC.get_Fields("ContractNo");
                    sOwner = clC.get_Fields("fname") + " " + clC.get_Fields("lname");
                    sVIN = clC.get_Fields("Vin");
                }

                SQL = "select * from servicecenter where servicecenterID = " + sServiceCenterID;
                clSC.OpenDB(SQL,sCON);
                if (clSC.RowCount > 0)
                {
                    clSC.GetRow();
                    sRepairSite = clSC.get_Fields("ServiceCenterName");
                    sAddress = clSC.get_Fields("Addr1");
                    try
                    {
                        sAddress = sAddress + " " + clSC.get_Fields("Addr2");
                    }
                    catch 
                    {

                        throw;
                    }//Addr2
                    sPostalCode = clSC.get_Fields("Zip");
                    sPhone = clSC.get_Fields("Phone");
                }




                var orderRequest = new OrderRequest
                {
                    
                    adjuster = sUserName,
                    adjusteremail = sUserEMail,
                    contractnumber = sContractNo,
                    authorizationnumber = sAuthorizationNumber,
                    repairordernumber = sRepairOrderNumber,
                    owner = sOwner,
                    vin = sVIN,
                    mileage = sMileage,
                    repairsite = sRepairSite,
                    address = sAddress,
                    postalcode = sPostalCode,
                    phone = sPhone,
                    contact = sContact,
                    warranty = sWarranty,
                    eocmileage = sEocMileage,
                    eocdate = sEocDate,
                    note = sNote,
                    parts = partRequests
                };
                var response = await restHelper.ServiceRequestObjectNoValidation<OrderRequest, OrderResponse>(httpClient, HttpMethod.Post, route, orderRequest, tokenProvider);

                return response;
            }

        }


        public static async Task<List<MakeItem>> GetMakes(IRestHelper restHelper, ITokenProvider tokenProvider, TimeSpan timeOut, string baseUrl, string route)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(baseUrl);
                httpClient.Timeout = timeOut;

                return await restHelper.ServiceRequestNoValidation<List<MakeItem>>(httpClient, HttpMethod.Get, route, new Dictionary<string, string>(), tokenProvider);

            }
        }


        public static async Task<QuoteResponse> GetQuote(IRestHelper restHelper, ITokenProvider tokenProvider, TimeSpan timeOut, string baseUrl, string route)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(baseUrl);
                httpClient.Timeout = timeOut;
                var partsList = new List<Part>();

                DBO.clsDBO clR = new DBO.clsDBO();
                string SQL;
                SQL = "select * from claimdetailpurchase " +
                    "where claimid = " + lClaimID;
                clR.OpenDB(SQL, sCON);
                if (clR.RowCount > 0)
                {
                    int counter = 0;
                    while (counter <= Convert.ToInt32(clR.RowCount) - 1)
                    {
                        clR.GetRowNo(counter);
                        counter++;
                        lClaimDetailID = int.Parse(clR.get_Fields("claimdetailid"));
                        partsList.Add(new Part {Partnumber = clR.get_Fields("partno"), Quantity = int.Parse(clR.get_Fields("qty")), Showaftermarketoption = true });
                    }
                }

                SQL = "select * from claim cl " +
                    "inner join contract c on c.contractid = cl.contractid " +
                    "where claimid = " + lClaimID;
                clR.OpenDB(SQL, sCON);
                if (clR.RowCount > 0)
                {
                    clR.GetRow();
                    sVIN = clR.get_Fields("vin");
                }

                var request = new QuoteRequest
                {
                    oemid = 3,
                    vin = sVIN,
                    parts = partsList
                };
                return await restHelper.ServiceRequestObject<QuoteRequest, QuoteResponse>(httpClient, HttpMethod.Post, route,request, tokenProvider);

            }
        }




        public static IRestHelper GetRestHelper()
        {
            return new RestHelper();
        }

        public static ITokenProvider GetTokenProvider(CKWebapiHTTPTokenProviderConfig config, IRestHelper restHelper)
        {
            return new CKWebapiHTTPTokenProvider(config, restHelper);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestDemo.Models
{


    public class OrderResponse
    {
        public string orderid { get; set; }
        public decimal oempartscost { get; set; }
        public decimal aftermarketpartscost { get; set; }
        public decimal oemwarrantycost { get; set; }
        public decimal aftermarketwarrantycost { get; set; }
        public decimal oemshippingcost { get; set; }
        public decimal aftermarketshippingcost { get; set; }
        public decimal oemcoreshippingcost { get; set; }
        public decimal aftermarketcoreshippingcost { get; set; }
        public decimal totalordercost { get; set; }
        public string error { get; set; }
    }

}

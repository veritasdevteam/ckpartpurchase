﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace RestDemo.Models
{
    class TokenResponse : CKWIS.Support.Models.ISelfValidate
    {
        public TokenResponse() { }
        public TokenResponse(string userName, string access_Token, int expires_In, DateTime issued, DateTime expires)
        {
            UserName = userName;
            Access_Token = access_Token;
            Expires_In = expires_In;
            Issued = issued;
            Expires = expires;
            SelfValidate(false, null, HttpStatusCode.OK);
        }


        [JsonProperty]
        public string UserName { get; private set; }
        [JsonProperty]
        public string Access_Token { get; private set; }
        [JsonProperty]
        public int Expires_In { get; private set; }
        [JsonProperty]
        public DateTime Issued { get; private set; }
        [JsonProperty]
        public DateTime Expires { get; private set; }

        

        public bool TokenExpired {
            get {
                return Expires >= DateTime.UtcNow;
            }
        }

        public bool ErrorEncountered { get; private set; }

        public Exception Ex { get; private set; }

        public HttpStatusCode ResponseCode { get; private set; }

        public void SelfValidate(bool error, Exception ex, HttpStatusCode responseCode)
        {
            ErrorEncountered = error;
            Ex = ex;
            ResponseCode = responseCode;
            Expires.ToUniversalTime();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestDemo.Models
{
    public class OrderRequest
    {
        public string adjuster { get; set; }
        public string adjusteremail { get; set; }
        public string contractnumber { get; set; }
        public string authorizationnumber { get; set; }
        public string repairordernumber { get; set; }
        public string owner { get; set; }
        public string vin { get; set; }
        public string mileage { get; set; }
        public string repairsite { get; set; }
        public string address { get; set; }
        public string postalcode { get; set; }
        public string phone { get; set; }
        public string contact { get; set; }
        public string warranty { get; set; }
        public string eocmileage { get; set; }
        public string eocdate { get; set; }
        public string note { get; set; }
        public OrderRequestPart[] parts { get; set; }
    }

}

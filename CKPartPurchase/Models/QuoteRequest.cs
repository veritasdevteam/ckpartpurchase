﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestDemo.Models
{
    class QuoteRequest
    {
        /// <summary>
        /// OEMID for the Make of the vehicle. Use the GetMakes method to get the list of IDs. Leave blank if supplying a VIN
        /// </summary>
        public int? oemid { get; set; }
        /// <summary>
        /// 17-character VIN. If OEMID is supplied, VIN will be ignored.
        /// </summary>
        public string vin { get; set; }
        /// <summary>
        /// List of parts 
        /// </summary>
        public List<Part> parts { get; set; }
    }
}

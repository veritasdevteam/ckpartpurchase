﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RestDemo.Models
{
    public class MakesResponse : CKWIS.Support.Models.ISelfValidate
    {
        public List<MakeItem> Makes { get; set; }
        public bool ErrorEncountered { get; private set; }

        public Exception Ex { get; private set; }

        public HttpStatusCode ResponseCode { get; private set; }

        public void SelfValidate(bool error, Exception ex, HttpStatusCode responseCode)
        {
            ErrorEncountered = error;
            Ex = ex;
            ResponseCode = responseCode;
            if (error)
            {
                Makes = new List<MakeItem>();
            }

        }
    }

}

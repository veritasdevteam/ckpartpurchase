﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestDemo.Models
{
    public class WarrantyItem
    {
        /// <summary>
        /// The type of warranty being offered
        /// </summary>
        public string WarrantyType { get; set; }

        /// <summary>
        /// The description for the warranty 
        /// </summary>
        public string WarrantyDescription { get; set; }

        /// <summary>
        /// The cost of the warranty
        /// </summary>
        public decimal WarrantyCost { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestDemo.Models
{
    public class AftermarketPartResponse
    {
        /// <summary>
        /// Quote ID to be used when ordering
        /// </summary>
        public string quoteid { get; set; }
        /// <summary>
        /// Part number searched for or the new part number if it has been superseded
        /// </summary>
        public string partnumber { get; set; }
        /// <summary>
        /// Part description
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// Customer Price of a single Part
        /// </summary>
        public string yourcost { get; set; }
        /// <summary>
        /// Core value for a single Part
        /// </summary>
        public string corevalue { get; set; }
        /// <summary>
        /// URL to a Part image, if any
        /// </summary>
        public string url { get; set; }
        /// <summary>
        /// Yes,No
        /// </summary>
        public string instock { get; set; }
        /// <summary>
        /// Expected delivery date if ordered prior to cutoff
        /// </summary>
        public string deliverydate { get; set; }
        /// <summary>
        /// Minutes until order cutoff for delivery date
        /// </summary>
        public string cutoff { get; set; }

        /// <summary>
        /// List of Warranty Options
        /// </summary>
        public List<WarrantyItem> warranty { get; set; }

    }

}

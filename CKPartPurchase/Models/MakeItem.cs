﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestDemo.Models
{
    public class MakeItem
    {
        public int oemid { get; set; }
        public string make { get; set; }
    }
}

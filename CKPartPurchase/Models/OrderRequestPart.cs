﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestDemo.Models
{

    public class OrderRequestPart
    {
        public string quoteid { get; set; }
        public string partnumber { get; set; }
        public int quantity { get; set; }
        public bool aftermarketoption { get; set; }
    }
}

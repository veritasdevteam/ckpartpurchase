﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestDemo.Models
{
    class CKWebapiHTTPTokenProviderConfig
    {


        public Uri BaseAddress { get; set; }
        public TimeSpan TokenTimeout { get; set; }
        public string TokenRoute { get; set; }
        public TokenRequest TokenReq { get; set; }
    }
}

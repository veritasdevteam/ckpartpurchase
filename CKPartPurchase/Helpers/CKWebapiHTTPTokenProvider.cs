﻿using CKWIS.Support.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using RestDemo.Models;
using System.Net;

namespace RestDemo.Helpers
{
    /// <summary>
    /// Token Helper.. Not 100% hardened aginst failure to generate tokens.. 
    /// </summary>
    class CKWebapiHTTPTokenProvider : CKWIS.Support.Helpers.ITokenProvider
    {

        public async Task<string> GetToken()
        {
            if (_TokenResponse.ErrorEncountered || _TokenResponse.TokenExpired)
            {
                return await GenerateNewToken();
            }
            else
            {
                return _TokenResponse.Access_Token;
            }
        }


        private async Task<string> GenerateNewToken()
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = _Config.BaseAddress;
                httpClient.Timeout = _Config.TokenTimeout;
                _TokenResponse = await _iRestHelper.ServiceRequestObject<TokenRequest, TokenResponse>(httpClient, HttpMethod.Post, _Config.TokenRoute, _Config.TokenReq);
            }
            if (_TokenResponse == null || (_TokenResponse.ErrorEncountered && _TokenResponse.Ex == null))
            {
                throw new Exception(string.Format("Unable To Generate Token got HTTP errorcode {0}", Enum.GetName(_TokenResponse.ResponseCode.GetType(), _TokenResponse.ResponseCode)));
            }
            else if ((_TokenResponse.ErrorEncountered && _TokenResponse.Ex != null))
            {
                throw _TokenResponse.Ex;
            }
            else
            {
                return _TokenResponse.Access_Token;
            }


        }
        private TokenResponse _TokenResponse;


        private CKWebapiHTTPTokenProviderConfig _Config;
        private CKWIS.Support.Helpers.IRestHelper _iRestHelper;

        public CKWebapiHTTPTokenProvider(CKWebapiHTTPTokenProviderConfig config, CKWIS.Support.Helpers.IRestHelper restClient)
        {
            _Config = config;
            _iRestHelper = restClient;
            GenerateNewToken().Wait();

        }
       
    }
}

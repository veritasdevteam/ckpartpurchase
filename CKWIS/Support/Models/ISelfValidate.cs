﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace CKWIS.Support.Models
{

    public interface ISelfValidate
    {
        void SelfValidate(bool error, Exception ex, HttpStatusCode responseCode);
        bool ErrorEncountered { get; }
        Exception Ex { get; }
        HttpStatusCode ResponseCode { get; }

    }

}

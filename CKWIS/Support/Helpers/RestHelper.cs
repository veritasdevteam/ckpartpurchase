﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CKWIS.Support.Models;
using Newtonsoft.Json;
using System.Net;

namespace CKWIS.Support.Helpers
{
    public class RestHelper : IRestHelper
    {
        /// <summary>
        /// /// Sends a Service Request <p/>
        /// Returns a Typed Object when dealing with JSON <p/>
        /// No Validation or Saftey 
        /// </summary>
        /// <typeparam name="Tinput"></typeparam>
        /// <typeparam name="Toutput"></typeparam>
        /// <param name="service"></param>
        /// <param name="httpMethod"></param>
        /// <param name="route"></param>
        /// <param name="input"></param>
        /// <param name="tokenProvider"></param>
        /// <returns></returns>
        public async Task<Toutput> ServiceRequestObjectNoValidation<Tinput, Toutput>(HttpClient service, HttpMethod httpMethod, string route, Tinput input, ITokenProvider tokenProvider = null) 
        {
            string responseString;
            HttpResponseMessage response;
            Toutput item;

            var jsonString = JsonConvert.SerializeObject(input);
            var requestMessage = new HttpRequestMessage(httpMethod, route);
            if (tokenProvider != null)
            {
                var token = await tokenProvider.GetToken();
                requestMessage.Headers.Add("Authorization", "Bearer " + token);
            }
            if (httpMethod != HttpMethod.Get)
            {
                requestMessage.Content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            }
            response = await service.SendAsync(requestMessage);
            if (response.IsSuccessStatusCode)
            {
                responseString = await response.Content.ReadAsStringAsync();
                item = JsonConvert.DeserializeObject<Toutput>(responseString);
                return item;
            }
            else
            {
                throw new Exception("Error Encountered in ServiceRequestObjectNoValidation");
            }



        }

        /// <summary>
        /// Sends a Service Request <p/>
        /// Returns a Typed Object when dealing with JSON <p/>
        /// No Validation or Saftey 
        /// </summary>
        /// <typeparam name="Toutput"></typeparam>
        /// <param name="service"></param>
        /// <param name="httpMethod"></param>
        /// <param name="route"></param>
        /// <param name="queryString"></param>
        /// <returns></returns>
        public async Task<Toutput> ServiceRequestNoValidation<Toutput>(HttpClient service, HttpMethod httpMethod, string route, Dictionary<string, string> queryString, ITokenProvider tokenProvider = null)
        {

            string responseString;
            HttpResponseMessage response;
            Toutput item;

            HttpRequestMessage requestMessage;

            if (queryString == null)
            {
                queryString = new Dictionary<string, string>();
            }
            requestMessage = new HttpRequestMessage(httpMethod, QueryHelpers.AddQueryString(route, queryString));
            if (tokenProvider != null)
            {
                var token = await tokenProvider.GetToken();
                requestMessage.Headers.Add("Authorization", "Bearer " + token);
            }
            response = await service.SendAsync(requestMessage);

            if (response.IsSuccessStatusCode)
            {
                responseString = await response.Content.ReadAsStringAsync();
                item = JsonConvert.DeserializeObject<Toutput>(responseString);
                return item;

            }
            else
            {
                throw new Exception("ServiceRequestNoValidation failed");
            }



        }

        /// <summary>
        /// Sends a Service Request <p/>
        /// Returns a Typed Object when dealing with JSON <p/>
        /// </summary>
        /// <typeparam name="Toutput"></typeparam>
        /// <param name="service"></param>
        /// <param name="httpMethod"></param>
        /// <param name="route"></param>
        /// <param name="queryString"></param>
        /// <returns></returns>
        public async Task<Toutput> ServiceRequest<Toutput>(HttpClient service, HttpMethod httpMethod, string route, Dictionary<string, string> queryString, ITokenProvider tokenProvider = null) where Toutput : ISelfValidate, new()
        {
            
            string responseString;
            HttpResponseMessage response;
            Toutput item;            
            try
            {
                HttpRequestMessage requestMessage;
                
                if (queryString == null)
                {
                    queryString = new Dictionary<string, string>();
                }
                requestMessage = new HttpRequestMessage(httpMethod, QueryHelpers.AddQueryString(route, queryString));
                if (tokenProvider != null)
                {
                    var token = await tokenProvider.GetToken();
                    requestMessage.Headers.Add("Authorization", "Bearer " + token);
                }
                response = await service.SendAsync(requestMessage);

                if (response.IsSuccessStatusCode)
                {
                    responseString = await response.Content.ReadAsStringAsync();
                    item = JsonConvert.DeserializeObject<Toutput>(responseString);
                    item.SelfValidate(false, null,response.StatusCode);
                }
                else
                {
                    item = new Toutput();
                    item.SelfValidate(true, null, response.StatusCode);
                }
                return item;
            }
            catch (TaskCanceledException CanEx)
            {
                item = new Toutput();
                item.SelfValidate(true, CanEx, HttpStatusCode.RequestTimeout);
                Console.WriteLine(CanEx);
                return item;
            }
            catch (HttpRequestException ReqEx)
            {
                item = new Toutput();
                item.SelfValidate(true, ReqEx, HttpStatusCode.RequestTimeout);
                Console.WriteLine(ReqEx);
                return item;
            }
            catch (Exception Ex)
            {
                item = new Toutput();
                item.SelfValidate(true, Ex, HttpStatusCode.RequestTimeout);
                Console.WriteLine(Ex);
                return item;
            }

        }

        /// <summary>
        /// Sends a Service Request <p/>
        /// Returns a Typed Object when dealing with JSON <p/>
        /// .. When an Exception occurs it will always set the HTTP Status code of ISelfValidate to Request Time Out. Check the EX for the spacific issue
        /// </summary>
        /// <typeparam name="Tinput"></typeparam>
        /// <typeparam name="Toutput"></typeparam>
        /// <param name="service"></param>
        /// <param name="httpMethod"></param>
        /// <param name="route"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<Toutput> ServiceRequestObject<Tinput, Toutput>(HttpClient service, HttpMethod httpMethod, string route, Tinput input, ITokenProvider tokenProvider = null) where Toutput : ISelfValidate, new()
        {
            string responseString;
            HttpResponseMessage response;
            Toutput item;
            try
            {
                var jsonString = JsonConvert.SerializeObject(input);
                var requestMessage = new HttpRequestMessage(httpMethod, route);
                if (tokenProvider != null)
                {
                    var token = await tokenProvider.GetToken();
                    requestMessage.Headers.Add("Authorization", "Bearer " + token);
                }
                if (httpMethod != HttpMethod.Get)
                {
                    requestMessage.Content = new StringContent(jsonString, Encoding.UTF8, "application/json");
                }
                response = await service.SendAsync(requestMessage);
                if (response.IsSuccessStatusCode)
                {
                    responseString = await response.Content.ReadAsStringAsync();
                    item = JsonConvert.DeserializeObject<Toutput>(responseString);
                    item.SelfValidate(false, null, response.StatusCode);
                }
                else
                {
                    item = new Toutput();
                    item.SelfValidate(true, null, response.StatusCode);
                }                
                return item;
            }
            catch (TaskCanceledException CanEx)
            {
                item = new Toutput();
                item.SelfValidate(true, CanEx, HttpStatusCode.RequestTimeout);
                Console.WriteLine(CanEx);
                return item;
            }
            catch (HttpRequestException ReqEx)
            {
                item = new Toutput();
                item.SelfValidate(true, ReqEx, HttpStatusCode.RequestTimeout);
                Console.WriteLine(ReqEx);
                return item;
            }
            catch (Exception Ex)
            {
                item = new Toutput();
                item.SelfValidate(true, Ex, HttpStatusCode.RequestTimeout);
                Console.WriteLine(Ex);
                return item;
            }
        }
        public async Task<bool> SendWithCheckSuccessOnly<T>(HttpClient service, HttpMethod httpMethod, string route, T input, ITokenProvider tokenProvider = null)
        {
            HttpResponseMessage response;
            try
            {
                var jsonString = JsonConvert.SerializeObject(input);
                var requestMessage = new HttpRequestMessage(httpMethod, route);
                if (tokenProvider != null)
                {
                    var token = await tokenProvider.GetToken();
                    requestMessage.Headers.Add("Authorization", "Bearer " + token);
                }

                if (httpMethod != HttpMethod.Get)
                {
                    requestMessage.Content = new StringContent(jsonString, Encoding.UTF8, "application/json");
                }
                response = await service.SendAsync(requestMessage);

                return response.IsSuccessStatusCode;
            }
            catch (TaskCanceledException CanEx)
            {
                Console.WriteLine(CanEx);
                return false;
            }
            catch (HttpRequestException ReqEx)
            {
                Console.WriteLine(ReqEx);
                return false;
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex);
                return false;
            }
        }
    }

}

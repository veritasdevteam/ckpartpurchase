﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKWIS.Support.Helpers
{
    public interface ITokenProvider
    {
        Task<string> GetToken();
    }
}

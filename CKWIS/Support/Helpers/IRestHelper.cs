﻿using CKWIS.Support.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CKWIS.Support.Helpers
{
    public interface IRestHelper
    {
        Task<Toutput> ServiceRequestObjectNoValidation<Tinput, Toutput>(HttpClient service, HttpMethod httpMethod, string route, Tinput input, ITokenProvider tokenProvider = null);
        Task<Toutput> ServiceRequestNoValidation<Toutput>(HttpClient service, HttpMethod httpMethod, string route, Dictionary<string, string> queryString, ITokenProvider tokenProvider = null) ;

        Task<Toutput> ServiceRequestObject<Tinput, Toutput>(HttpClient service, HttpMethod httpMethod, string route, Tinput input, ITokenProvider tokenProvider = null) where Toutput : ISelfValidate, new();
        Task<Toutput> ServiceRequest<Toutput>(HttpClient service, HttpMethod httpMethod, string route, Dictionary<string, string> queryString, ITokenProvider tokenProvider = null) where Toutput : ISelfValidate, new();
        Task<bool> SendWithCheckSuccessOnly<T>(HttpClient service, HttpMethod httpMethod, string route, T input,ITokenProvider tokenProvider= null);


    }
}
